#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
    def test_read1(self):
        s = "1 100\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 100)
    def test_read2(self):  
        s = "100 200\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 100)
        self.assertEqual(j, 200)
    def test_read3(self): 
        s = "1 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 1)
    def test_read4(self):  
        s = "1000 1001\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1000)
        self.assertEqual(j, 1001)
    def test_read5(self): 
        s = "5654 6457\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 5654)
        self.assertEqual(j, 6457)
    def test_read6(self):    
        s = "78 89\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 78)
        self.assertEqual(j, 89)
    def test_read7(self): 
        s = "67 67\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 67)
        self.assertEqual(j, 67)
    def test_read8(self): 
        s = "3445467 8865645\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 3445467)
        self.assertEqual(j, 8865645)
    def test_read9(self): 
        s = "123 1567\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 123)
        self.assertEqual(j, 1567)
    def test_read10(self): 
        s = "1 100000\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 100000)



    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(2010, 2100)
        self.assertEqual(v, 157)

    def test_eval_6(self):
        v = collatz_eval(3455, 21086)
        self.assertEqual(v, 279)

    def test_eval_7(self):
        v = collatz_eval(1233, 7656)
        self.assertEqual(v, 262)

    def test_eval_8(self):
        v = collatz_eval(1337, 42069)
        self.assertEqual(v, 324)
    # -----
    # print
    # -----

    def test_print1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
    def test_print2(self):  
        w = StringIO()
        collatz_print(w, 1, 1, 1)
        self.assertEqual(w.getvalue(), "1 1 1\n")
    def test_print3(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")
    def test_print4(self):
        w = StringIO()
        collatz_print(w, 900, 1000, 174)
        self.assertEqual(w.getvalue(), "900 1000 174\n")
    def test_print5(self):
        w = StringIO()
        collatz_print(w, 2010, 2100, 157)
        self.assertEqual(w.getvalue(), "2010 2100 157\n")
    def test_print6(self):
        w = StringIO()
        collatz_print(w, 3455, 21086, 279)
        self.assertEqual(w.getvalue(), "3455 21086 279\n")
    def test_print7(self):
        w = StringIO()
        collatz_print(w, 1337, 42069, 324)
        self.assertEqual(w.getvalue(), "1337 42069 324\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n900 1000 174\n")
    def test_solve1(self):        
        r = StringIO("2010 2100\n3455 21086\n1337 42069\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "2010 2100 157\n3455 21086 279\n1337 42069 324\n")
    def test_solve2(self):
        r = StringIO("1 1\n1 200\n210 210\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1 1\n1 200 125\n210 210 40\n")
    def test_solve3(self):
        r = StringIO("100000 100001\n100100 100201\n101100 102201\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "100000 100001 129\n100100 100201 341\n101100 102201 310\n")
    def test_solve4(self):
        r = StringIO("765 102298\n76532 129889\n7532 12989\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "765 102298 351\n76532 129889 354\n7532 12989 268\n")

# ----
# main
# ----

if __name__ == "__main__": #pragma: no cover
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
